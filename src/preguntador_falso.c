#include "preguntador.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *preguntarNombre() {
  char *nombre = malloc(sizeof(char) * TAMANIO_MAX_NOMBRE);
  if (nombre == NULL) {
    return NULL;
  }

  strcpy(nombre, "Ana Perez");

  return nombre;
}
