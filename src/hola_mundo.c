#include "hola_mundo.h"
#include "preguntador.h"
#include <stdio.h>
#include <stdlib.h>

void saludar() {
  char *nombre = preguntarNombre();
  if (nombre == NULL) {
    printf("Error al preguntar el nombre!");
    return;
  }

  printf("Hola mundo %s!\n", nombre);
  free(nombre);
}
