#include "preguntador.h"
#include <stdio.h>
#include <stdlib.h>

char *preguntarNombre() {
  char *nombreIngresado = malloc(sizeof(char) * TAMANIO_MAX_NOMBRE);
  if (nombreIngresado == NULL) {
    return NULL;
  }

  printf("Ingresar el nombre: ");
  scanf("%s", nombreIngresado);

  return nombreIngresado;
}
