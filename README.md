# Ejemplo práctico de reutilización de código

Para empezar es necesario compilar los archivos de código, generando los archivos objeto:

```
vagrant@ubuntu-bionic:~$ cd clase_reutilizacion_codigo/out/
vagrant@ubuntu-bionic:~/clase_reutilizacion_codigo/out$ gcc -c ../src/*.c
```

## Compilación con librería estática

### Usando los archivos objeto por separado

Para compilar utilizando `preguntador_desde_stdin`:
```
gcc main.o hola_mundo.o preguntador_desde_stdin.o -o main_desde_stdin
```

Para compilar utilizando `preguntador_falso`:
```
gcc main.o hola_mundo.o preguntador_falso.o -o main_falso
```

### Empaquetando la librería

Para generar la librería estática `libhola_desde_stdin_est.a`:
```
ar rcs libhola_desde_stdin_est.a hola_mundo.o preguntador_desde_stdin.o
nm libhola_desde_stdin_est.a
```

Para compilar utilizando `preguntador_desde_stdin_est`:
```
gcc main.o -L. -lhola_desde_stdin_est -o main_desde_stdin_con_lib_est
```

Para generar la librería estática `libhola_falso_est.a`:
```
ar rcs libhola_falso_est.a hola_mundo.o preguntador_falso.o
nm libhola_falso_est.a
```

Para compilar utilizando `preguntador_falso_est`:
```
gcc main.o -L. -lhola_falso_est -o main_falso_con_lib_est
```

## Compilación con librería dinámica

Para generar la librería dinámica `libhola_desde_stdin_din.so`:
```
ld -o libhola_desde_stdin_din.so hola_mundo.o preguntador_desde_stdin.o -shared
```

Para generar el ejecutable:
```
gcc main.o -L. -lhola_desde_stdin_din -o main_desde_stdin_con_lib_din
```

Para ejecutarlo tenemos que agregar el directorio donde está la librería dinámica a la lista de lugares donde la buscará al ejecutar el programa:
```
LD_LIBRARY_PATH=`pwd`":$LD_LIBRARY_PATH" ./main_desde_stdin_con_lib_din
```

Podemos ver las dependencias dinámicas del programa con `ldd`:
```
vagrant@ubuntu-bionic:~/clase_reutilizacion_codigo/out$ ldd main_desde_stdin_con_lib_din
	linux-vdso.so.1 (0x00007ffc93d8d000)
	libhola_desde_stdin_din.so => not found
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f3ff837f000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f3ff8972000)
```

Para generar la librería dinámica `libhola_falso_din.so`:
```
ld -o libhola_falso_din.so hola_mundo.o preguntador_falso.o -shared
```

Para generar el ejecutable:
```
gcc main.o -L. -lhola_falso_din -o main_falso_con_lib_din
```

Para ejecutarlo tenemos que agregar el directorio donde está la librería dinámica a la lista de lugares donde la buscará al ejecutar el programa:
```
LD_LIBRARY_PATH=`pwd`":$LD_LIBRARY_PATH" ./main_falso_con_lib_din
```

Finalmente, podemos compilar las librerías dinámicas en forma estática:
```
gcc src/main.c src/hola_mundo.c src/preguntador_desde_stdin.c -o out/main_desde_stdin_estatico_2 -static
vagrant@ubuntu-bionic:~/clase_reutilizacion_codigo ldd out/main_desde_stdin_estatico_2
	not a dynamic executable
```

### Cambiando la librería en tiempo de ejecución

Volvamos a compilar las librerías dinámicas con el mismo nombre y en diferentes subdirectorios:
```
mkdir desde_stdin
ld -o desde_stdin/libhola.so hola_mundo.o preguntador_desde_stdin.o -shared
mkdir falso
ld -o falso/libhola.so hola_mundo.o preguntador_falso.o -shared
gcc main.o -L./falso -lhola -o main_din
```

Ahora para ejecutarlo usando la librería `falsa`:
```
LD_LIBRARY_PATH=`pwd`/falso ./main_din
```

Para ejecutarlo usando la librería `desde_stdin`:
```
LD_LIBRARY_PATH=`pwd`/desde_stdin ./main_din
```
